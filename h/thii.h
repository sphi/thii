#ifndef THII_H
#define THII_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "define_EVIL.h"

#define MAYBE_UNUSED __attribute__((unused))

#define FUNCTION_ARGUMENTS ( \
    FunctionValue const* _current_function MAYBE_UNUSED, \
    struct ArgumentList _arguments MAYBE_UNUSED)

#define MAKE(...) ((typeof(__VA_ARGS__)*)memcpy( \
    malloc(sizeof(__VA_ARGS__)), \
    &(__VA_ARGS__), sizeof(__VA_ARGS__)))

typedef struct Value Value;
typedef struct FunctionValue FunctionValue;
typedef struct ArgumentList ArgumentList;
typedef struct ScopeInfo ScopeInfo;

typedef struct FunctionValue {
    unsigned char arg_count;
    Value (*fn_ptr) FUNCTION_ARGUMENTS;
    const char* name;
    struct {
        const char* name;
        ScopeInfo* scopes;
    } closure;
} FunctionValue;

typedef struct Value {
    struct Value* link; // Used internally for linked list
    enum {
        VALUE_NIL,
        VALUE_BOOL,
        VALUE_NUMBER,
        // Those above can be converted to a number
        VALUE_STR,
        VALUE_FUN,
        VALUE_PAIR,
    } tag;
    union {
        bool boolean;
        double number;
        const char* str;
        struct Pair* pair;
        FunctionValue const* fun;
    } data;
} Value;

#define VALUE_ASSIGN_DATA(dest, source) \
    (dest).tag = (source).tag; \
    (dest).data = (source).data; \

struct Pair {
    Value x;
    Value y;
};

#define MAX_ARGUMENTS 12 // NOTE: you must add macros to THII_ARGUMENT_* if increasing
typedef struct ArgumentList {
    unsigned char count;
    Value args[MAX_ARGUMENTS];
} ArgumentList;

typedef struct ScopeInfo {
    ScopeInfo* parent;
    ScopeInfo* child; // null on local instances, only used for closure scopes
    int id; // index of this scope among siblings
    Value* bindings; // iinked list head
} ScopeInfo;

static inline Value value_make_number(double val) {
    return (Value){.tag = VALUE_NUMBER, .data.number = val};
}
static inline Value value_make_bool(bool val) {
    return (Value){.tag = VALUE_BOOL, .data.boolean = val};
}
static inline Value value_make_str(const char* val) {
    return (Value){.tag = VALUE_STR, .data.str = val};
}
static inline Value value_make_pair(struct Pair* val) {
    return (Value){.tag = VALUE_PAIR, .data.pair = val};
}
static inline Value value_make_value(Value val) {
    return val;
}
#define nil (Value){.tag = VALUE_NIL}

Value panic(const char* msg) {
    fprintf(stderr, "panic: %s\n", msg);
    exit(1);
    return (Value){0};
}

static inline char value_get_bool(Value v) {
    switch (v.tag) {
        case VALUE_NIL: return false;
        case VALUE_BOOL: return v.data.boolean;
        case VALUE_NUMBER: return v.data.number != 0;
        case VALUE_STR: return v.data.str != 0 && *v.data.str != 0;
        case VALUE_PAIR: return true;
        case VALUE_FUN: return true;
    }
    panic("cannot convert unknown type to boolean"); return 0;
}

static inline double value_get_double(Value v) {
    switch (v.tag) {
        case VALUE_NIL: return 0;
        case VALUE_BOOL: return v.data.boolean;
        case VALUE_NUMBER: return v.data.number;
        case VALUE_STR: panic("cannot convert string to number"); return 0;
        case VALUE_PAIR: panic("cannot convert pair to number"); return 0;
        case VALUE_FUN: panic("cannot convert function to number"); return 0;
    }
    panic("cannot convert unknown type to float"); return 0;
}

static inline FunctionValue const* value_get_function(Value v) {
    return v.tag == VALUE_FUN ? v.data.fun : nullptr;
}

#define WRITE_LIT(lit) cb(lit, sizeof(lit) - 1, data)
#define BUFFER_SIZE 50
#define WRITE_FMT(...) written_size = snprintf(buffer, BUFFER_SIZE, __VA_ARGS__); \
    if (written_size < BUFFER_SIZE - 1) { cb(buffer, written_size, data); } \
    else { panic("string buffer too small"); }

static void value_print_to(
    Value v,
    bool toplevel,
    void (*cb)(const char* str, int len, void* data),
    void* data
) {
    char buffer[BUFFER_SIZE];
    int written_size;
    switch (v.tag) {
        case VALUE_NIL: WRITE_LIT("nil"); return;
        case VALUE_BOOL: v.data.boolean ? WRITE_LIT("true") : WRITE_LIT("false"); return;
        case VALUE_NUMBER: WRITE_FMT("%.6g", v.data.number); return;
        case VALUE_STR:
            if (toplevel) {
                cb(v.data.str, strlen(v.data.str), data);
            } else {
                WRITE_LIT("\"");
                for (const char* c = v.data.str; *c; c++) {
                    switch (*c) {
                        case '"': WRITE_LIT("\\\""); break;
                        case '\n': WRITE_LIT("\\n"); break;
                        case '\\': WRITE_LIT("\\\\"); break;
                        default: cb(c, 1, data);
                    }
                }
                WRITE_LIT("\"");
            }
            return;

        case VALUE_PAIR:
            WRITE_LIT("(");
            value_print_to(v.data.pair->x, false, cb, data);
            WRITE_LIT(", ");
            value_print_to(v.data.pair->y, false, cb, data);
            WRITE_LIT(")");
            return;

        case VALUE_FUN:
            cb(v.data.fun->name, strlen(v.data.fun->name), data);
            if (v.data.fun->closure.name) {
                WRITE_LIT(":");
                cb(v.data.fun->closure.name, strlen(v.data.fun->closure.name), data);
            } else if (v.data.fun->closure.scopes) {
                WRITE_LIT(":<closure>");
            }
            WRITE_LIT("()");
            return;
    }

    WRITE_FMT("<unkown type %d>", v.tag);
}

#undef WRITE_LIT
#undef BUFFER_SIZE
#undef WRITE_FMT

static void fmt_values(
    const char* fmt,
    Value* v,
    int v_len,
    void (*cb)(const char* str, int len, void* data),
    void* data
) {
    while (fmt && *fmt) {
        const char* end = fmt;
        while (*end && *end != '%') { end++; }
        if (end > fmt) { cb(fmt, end - fmt, data); }
        if (*end == '%') {
            end++;
            switch (*end) {
                case 's':
                case 'p':
                    if (v_len > 0) {
                        value_print_to(*v, *end == 's', cb, data); v++; v_len--;
                    } else {
                        fprintf(stderr, "WARNING: not enough values for format string\n");
                    }
                    break;
                case '%': cb("%", 1, data); break;
                default:
                    fprintf(stderr, "WARNING: invalid format character %c\n", *end);
            }
            end++;
        }
        fmt = end;
    }
    if (v_len > 0) {
        fprintf(stderr, "WARNING: too many values for format string\n");
    }
}

static void print_callback(const char* str, int len, void* data) {
    fprintf(data, "%.*s", len, str);
}

static void fmt_count_callback(const char* str, int len, void* data) {
    (void)str;
    (*(int*)data)+= len;
}

static void fmt_write_callback(const char* str, int len, void* data) {
    memcpy(*(char**)data, str, len);
    (*(char**)data) += len;
}

static inline void print_fmt_values(const char* fmt, Value* v, int v_len) {
    fmt_values(fmt, v, v_len, print_callback, stdout);
}

static inline const char* fmt_new_string(const char* fmt, Value* v, int v_len) {
    int len = 0;
    fmt_values(fmt, v, v_len, fmt_count_callback, &len);
    char* buf = malloc(len + 1);
    const char* ret = buf;
    fmt_values(fmt, v, v_len, fmt_write_callback, &buf);
    *buf = '\0';
    if (buf != ret + len) {
        panic("format logic error");
    }
    return ret;
}

typedef enum OpId {
    OP_PLUS = 35,
    OP_MINUS = 29,
    OP_TIMES = 96,
    OP_DIVIDE = 10,
    OP_MOD = 2,
} OpId;
#define OP_TO_OP_ID(token) (OpId)(32 token 3)

static inline double apply_op_double(OpId op_id, double first, double second) {
    switch (op_id) {
        case OP_PLUS: return first + second;
        case OP_MINUS: return first - second;
        case OP_TIMES: return first * second;
        case OP_DIVIDE: return first / second;
        case OP_MOD: return fmod(first, second);
        default: panic("unknown op"); return 0;
    }
}

static inline Value apply_op_value(OpId op_id, Value first, Value second) {
    if (first.tag <= VALUE_NUMBER && second.tag <= VALUE_NUMBER) {
        return value_make_number(apply_op_double(op_id, value_get_double(first), value_get_double(second)));
    } else if (op_id == OP_PLUS && (first.tag == VALUE_STR || second.tag == VALUE_STR)) {
        return value_make_str(fmt_new_string("%s%s", (Value[]){first, second}, 2));
    } else {
        panic("invalid op");
        return nil;
    }
}

static inline Value call_function_value(Value func, ArgumentList args) {
    if (func.tag != VALUE_FUN) {
        panic("tried to call non-function");
    }
    return func.data.fun->fn_ptr(func.data.fun, args);
}

static inline ScopeInfo* copy_scope_stack(ScopeInfo* scope_source) {
    // TODO: refactor to allocate in a single block
    ScopeInfo* scope_copy_head = nullptr;
    ScopeInfo* scope_copy_prev = nullptr;
    ScopeInfo** scope_copy = &scope_copy_head;
    while (scope_source) {
        Value* bindings_copy_head = nullptr;
        Value* bindings_source = scope_source->bindings;
        Value** bindings_copy = &bindings_copy_head;
        while (bindings_source) {
            *bindings_copy = MAKE(*bindings_source);
            bindings_copy = &(*bindings_copy)->link;
            bindings_source = bindings_source->link;
        }
        *scope_copy = MAKE((ScopeInfo){
            .parent = nullptr,
            .child = scope_copy_prev,
            .id = scope_source->id,
            .bindings = bindings_copy_head
        });
        scope_copy_prev = *scope_copy;
        scope_copy = &(*scope_copy)->parent;
        scope_source = scope_source->parent;
    }
    return scope_copy_head;
}

static inline void hydrate_scope_stack(ScopeInfo* dest, ScopeInfo* source) {
#define SCOPE_STACK_ASSERT(cond) if (!(cond)) panic("invalid scope stack")
    while (dest) {
        SCOPE_STACK_ASSERT(source && dest->id == source->id);
        Value* dest_value = dest->bindings;
        Value* source_value = source->bindings;
        while (dest_value) {
            SCOPE_STACK_ASSERT(source_value);
            VALUE_ASSIGN_DATA(*dest_value, *source_value)
            dest_value->tag = source_value->tag;
            dest_value->data = source_value->data;
            dest_value = dest_value->link;
            source_value = source_value->link;
        }
        SCOPE_STACK_ASSERT(!source_value);
        dest = dest->parent;
        source = source->parent;
    }
    SCOPE_STACK_ASSERT(!source);
#undef SCOPE_STACK_ASSERT
}

// # Map arguments helper
// ======================
// Normally this would be done with the EVIL_MAP family of macros, but sometimes we have an unkown number of arguments
// each with an unkown number of expressions. Since macros can't be recursive, we need another set.
#define THII_ARGUMENT_0(...)
#define THII_ARGUMENT_1(macro, index, expr, ...) macro(expr, index)
#define THII_ARGUMENT_2(macro, index, expr, ...) macro(expr, index) THII_ARGUMENT_1(macro, EVIL_EXPAND_CAT(EVIL_INC_, index), __VA_ARGS__)
#define THII_ARGUMENT_3(macro, index, expr, ...) macro(expr, index) THII_ARGUMENT_2(macro, EVIL_EXPAND_CAT(EVIL_INC_, index), __VA_ARGS__)
#define THII_ARGUMENT_4(macro, index, expr, ...) macro(expr, index) THII_ARGUMENT_3(macro, EVIL_EXPAND_CAT(EVIL_INC_, index), __VA_ARGS__)
#define THII_ARGUMENT_5(macro, index, expr, ...) macro(expr, index) THII_ARGUMENT_4(macro, EVIL_EXPAND_CAT(EVIL_INC_, index), __VA_ARGS__)
#define THII_ARGUMENT_6(macro, index, expr, ...) macro(expr, index) THII_ARGUMENT_5(macro, EVIL_EXPAND_CAT(EVIL_INC_, index), __VA_ARGS__)
#define THII_ARGUMENT_7(macro, index, expr, ...) macro(expr, index) THII_ARGUMENT_6(macro, EVIL_EXPAND_CAT(EVIL_INC_, index), __VA_ARGS__)
#define THII_ARGUMENT_8(macro, index, expr, ...) macro(expr, index) THII_ARGUMENT_7(macro, EVIL_EXPAND_CAT(EVIL_INC_, index), __VA_ARGS__)
#define THII_ARGUMENT_9(macro, index, expr, ...) macro(expr, index) THII_ARGUMENT_8(macro, EVIL_EXPAND_CAT(EVIL_INC_, index), __VA_ARGS__)
#define THII_ARGUMENT_10(macro, index, expr, ...) macro(expr, index) THII_ARGUMENT_9(macro, EVIL_EXPAND_CAT(EVIL_INC_, index), __VA_ARGS__)
#define THII_ARGUMENT_11(macro, index, expr, ...) macro(expr, index) THII_ARGUMENT_10(macro, EVIL_EXPAND_CAT(EVIL_INC_, index), __VA_ARGS__)
#define THII_ARGUMENT_12(macro, index, expr, ...) macro(expr, index) THII_ARGUMENT_11(macro, EVIL_EXPAND_CAT(EVIL_INC_, index), __VA_ARGS__)
#define THII_MAP_ARGUMENTS(macro, ...) EVIL_EXPAND_CAT(THII_ARGUMENT_, EVIL_COUNT(__VA_ARGS__))(EVIL_EXPAND macro, 0, __VA_ARGS__)

// # Value
// =======
// This uses the _Generic() C feature to create a value struct out of various types of literals and variables
#define VALUE_CODEGEN(value) \
    _expr_result = _Generic((value), \
        bool: value_make_bool, \
        int: value_make_number, \
        long: value_make_number, \
        double: value_make_number, \
        char*: value_make_str, \
        Value: value_make_value \
    )(value);

// # Seek
// ======
// The seek system is wha allows control flow to navigate to the correct place after having entered the function as a
// closure.
#define NEXT_SCOPE_ID() \
    (_seek_scope ? (_scope_id_tmp = _seek_scope->id, _seek_scope = _seek_scope->child, _scope_id_tmp) : 0)

#define SEEK_INDEX(index) /* fallthrough */ case index: _scope_info.id = index;
#define FORWARD_SEEK_INDEX_CODEGEN(index)
#define TOPLEVEL_SEEK_INDEX_CODEGEN(index)
#define DECLARE_SEEK_INDEX_CODEGEN(index)
#define SCOPED_SEEK_INDEX_CODEGEN(index) SEEK_INDEX(index)

#define SEEKABLE_SCOPE(...) \
    { \
        ScopeInfo* const _parent_scope_info_tmp = &_scope_info; \
        { \
            ScopeInfo _scope_info MAYBE_UNUSED = { \
                .parent = _parent_scope_info_tmp, \
                .id = 0, \
            }; \
            switch (NEXT_SCOPE_ID()) { \
                case 0: __VA_ARGS__; break; \
                default: panic("invalid seek index"); \
            } \
        } \
    }

// # Expression
// ============
// Expressions can be a naked literal/variable, a single form or a list of values/forms. The one restriction is lists
// cannot directly contain other lists (this is not possible to handle since macros can't be recursive). The common
// entry point is SCOPED_EXPR_CODEGEN(). ctx is the context (TOPLEVEL_, SCOPED_, etc), which is prepended to the start
// of each sub-expression in order to expand different versions of the macro depending on context. You can think of this
// as a sort of macro polymorphism.
#define EVIL_ENABLE_EQ_SCOPED__SCOPED_
#define EXPR_LIST_ITEM_CODEGEN(ctx, expr, index) \
    EVIL_IF_ELSE(EVIL_HAS_PEREN(expr)) ( \
        EVIL_EXPAND_CAT(ctx, SEEK_INDEX_CODEGEN)(EVIL_INC_##index) \
        EVIL_IF_ELSE(EVIL_HAS_PEREN(EVIL_EXPAND expr)) ( \
            EVIL_EXPAND_CALL(EVIL_EXPAND_CAT, ctx, EVIL_EXPAND EVIL_EXPAND expr) \
        )( \
            EVIL_EXPAND_CALL(EVIL_EXPAND_CAT, ctx, EVIL_EXPAND expr) \
        ) \
    )( \
        EVIL_EXPAND_CAT(ctx, SCOPED_ONLY_CODEGEN)(UNBOUND_VALUE, VALUE_CODEGEN(expr)) \
    )

#define TOPLEVEL_EXPR_CODEGEN(expr) \
    EVIL_MAP((EXPR_LIST_ITEM_CODEGEN, FORWARD_), EVIL_EXPAND expr) \
    EVIL_MAP((EXPR_LIST_ITEM_CODEGEN, TOPLEVEL_), EVIL_EXPAND expr)

#define SCOPED_EXPR_CODEGEN_WITHOUT_HYDRATE(...) \
    SEEKABLE_SCOPE( \
        EVIL_MAP((EXPR_LIST_ITEM_CODEGEN, SCOPED_), __VA_ARGS__) \
    ) \

#define SCOPED_EXPR_CODEGEN_WITH_HYDRATE(...) \
    { \
        EVIL_MAP((EXPR_LIST_ITEM_CODEGEN, DECLARE_), __VA_ARGS__) \
        SCOPED_EXPR_CODEGEN_WITHOUT_HYDRATE(__VA_ARGS__) \
    }

#define SCOPED_EXPR_CODEGEN(expr) \
    EVIL_IF_ELSE(EVIL_HAS_PEREN(expr)) ( \
        EVIL_IF_ELSE(EVIL_HAS_PEREN(EVIL_EXPAND expr)) ( \
            EVIL_EXPAND_CALL(EVIL_EXPAND_CAT, SCOPED_, EVIL_EXPAND EVIL_EXPAND expr) \
        )( \
            EVIL_IF_ELSE(EVIL_IS_THING(EVIL_MAP( \
                (EXPR_LIST_ITEM_CODEGEN, DECLARE_), \
                EVIL_EXPAND expr \
            ))) ( \
                SCOPED_EXPR_CODEGEN_WITH_HYDRATE expr \
            )( \
                SCOPED_EXPR_CODEGEN_WITHOUT_HYDRATE expr \
            ) \
        ) \
    )( \
        VALUE_CODEGEN(expr) \
    )

// # Scoped-only helper
// ====================
// Convenient for setting up forms that should only emit code in a normal scope, and error if used outside any function
#define FORWARD_SCOPED_ONLY_CODEGEN(type, ...)
#define TOPLEVEL_SCOPED_ONLY_CODEGEN(type, ...) type##_NOT_ALLOWED_AT_TOPLEVEL
#define DECLARE_SCOPED_ONLY_CODEGEN(type, ...)
#define SCOPED_SCOPED_ONLY_CODEGEN(type, ...) __VA_ARGS__

// # Bind
// ======
// Bind is not directly exposed, but is used by let, defun, etc
#define FORWARD_BIND_CODEGEN(type, name, expr_code)
#define TOPLEVEL_BIND_CODEGEN(type, name, expr_code) type##_NOT_ALLOWED_AT_TOPLEVEL
#define DECLARE_BIND_CODEGEN(type, name, expr_code) \
    Value name = {.link = _scope_info.bindings}; \
    _scope_info.bindings = &name;
#define SCOPED_BIND_CODEGEN(type, name, expr_code) \
    EVIL_EXPAND expr_code \
    VALUE_ASSIGN_DATA(name, _expr_result)

// # Let
// =====
#define LET_SETUP(empty, name, expr) (BIND_CODEGEN(LET, name, (SCOPED_EXPR_CODEGEN(expr)))) ) // unbalanced
#define let LET_SETUP(

// # Defun
// =======
#define FORWARD_DEFUN_CODEGEN(fn_name, fn_arg_count, body_code) \
    static Value fn_name##_impl_ FUNCTION_ARGUMENTS; \
    static const FunctionValue fn_name##_value_ = { \
        .arg_count = fn_arg_count, .fn_ptr = fn_name##_impl_, .name = #fn_name \
    }; \
    static const Value fn_name MAYBE_UNUSED = { \
        .tag = VALUE_FUN, \
        .data.fun = &fn_name##_value_, \
    };

#define TOPLEVEL_DEFUN_CODEGEN_BODY(body_code) \
    int _scope_id_tmp; \
    Value _expr_result = {0}; \
    ScopeInfo _scope_info MAYBE_UNUSED = {0}; \
    switch (0) { case 0: \
        EVIL_EXPAND body_code \
    }

#define TOPLEVEL_DEFUN_CODEGEN(fn_name, fn_arg_count, body_code) \
    static Value fn_name##_impl_ FUNCTION_ARGUMENTS { \
        ScopeInfo* _seek_scope = _current_function->closure.scopes; \
        while (_seek_scope && _seek_scope->parent && _seek_scope->parent->parent) { \
            _seek_scope = _seek_scope->parent; \
        } \
        TOPLEVEL_DEFUN_CODEGEN_BODY(body_code) \
        return _expr_result; \
    }

#define DECLARE_DEFUN_CODEGEN(fn_name, fn_arg_count, body_code) \
    DECLARE_BIND_CODEGEN(DEFUN, fn_name, (_expr_result = nil;))

#define SCOPED_DEFUN_CODEGEN(fn_name, fn_arg_count, body_code) \
    SEEKABLE_SCOPE ( \
        SEEK_INDEX(1) \
            SCOPED_BIND_CODEGEN(DEFUN, fn_name, (_expr_result = nil;)) \
            { \
                FunctionValue* _closure = memcpy(malloc(sizeof(FunctionValue)), _current_function, sizeof(FunctionValue)); \
                VALUE_ASSIGN_DATA(fn_name, ((Value){.tag = VALUE_FUN, .data.fun = _closure})); \
                _closure->closure.name = #fn_name; \
                _closure->closure.scopes = copy_scope_stack(&_scope_info); \
                _closure->closure.scopes->id++; \
            } \
        if (0) { SEEK_INDEX(2) SEEKABLE_SCOPE( \
            SEEK_INDEX(3) hydrate_scope_stack(_scope_info.parent, _current_function->closure.scopes); \
            SEEK_INDEX(4) EVIL_EXPAND body_code \
            return _expr_result; \
        )} \
    )

#define DEFUN_SETUP_ARGUMENT(name, index) \
    (BIND_CODEGEN(ARG, name, ( \
        _expr_result = index < _arguments.count ? _arguments.args[index] : nil; \
    ))),

#define DEFUN_SETUP(empty, fn_name, arg_list, body) (DEFUN_CODEGEN( \
    fn_name, \
    EVIL_COUNT arg_list, \
    ( \
        SCOPED_EXPR_CODEGEN(( \
            EVIL_MAP((DEFUN_SETUP_ARGUMENT), EVIL_EXPAND arg_list) \
            EVIL_IF_ELSE(EVIL_HAS_PEREN(body))(EVIL_EXPAND body)(body) \
        )) \
    ) \
)) ) // unbalanced

#define defun DEFUN_SETUP(

// # Call
// ======
#define CALL_SETUP_ARGUMENT(expr, index) \
    SEEK_INDEX(EVIL_EXPAND_CAT(EVIL_INC_, EVIL_INC_##index)) \
        SCOPED_EXPR_CODEGEN(expr) \
        _call_arguments.args[index] = _expr_result;

#define CALL_SETUP(empty, func, ...) (SCOPED_ONLY_CODEGEN(CALL, { \
    FunctionValue const* _call_callable = nullptr; \
    ArgumentList _call_arguments = { .count = EVIL_COUNT(__VA_ARGS__) }; \
    SEEKABLE_SCOPE( \
        SEEK_INDEX(1) \
            SCOPED_EXPR_CODEGEN(func) \
            _call_callable = value_get_function(_expr_result); \
            if (!_call_callable) panic(#func " is not callable"); \
        THII_MAP_ARGUMENTS((CALL_SETUP_ARGUMENT), __VA_ARGS__) \
        _expr_result = _call_callable->fn_ptr(_call_callable, _call_arguments); \
    ) \
})) ) // unbalanced

#define call CALL_SETUP(

// # If/else
// =========
#define IFEL_SETUP(empty, cond, yes, no) (SCOPED_ONLY_CODEGEN(IFEL, \
    SEEKABLE_SCOPE( \
        SEEK_INDEX(1) SCOPED_EXPR_CODEGEN(cond) \
        if (value_get_bool(_expr_result)) { \
            SEEK_INDEX(2) SCOPED_EXPR_CODEGEN(yes) \
        } else { \
            SEEK_INDEX(3) SCOPED_EXPR_CODEGEN(no) \
        } \
    ) \
)) ) // unbalanced

#define ifel IFEL_SETUP(

// # Operators
// ===========
#define OP_SETUP(op_token, expr_a, expr_b) (SCOPED_ONLY_CODEGEN(OPERATOR, { \
    Value _value_a = {0}; \
    SEEKABLE_SCOPE( \
        SEEK_INDEX(1) SCOPED_EXPR_CODEGEN(expr_a) \
        _value_a = _expr_result; \
        SEEK_INDEX(2) SCOPED_EXPR_CODEGEN(expr_b) \
        _expr_result = apply_op_value(OP_TO_OP_ID(op_token), _value_a, _expr_result); \
    ) \
})) ) // unbalanced

#define op OP_SETUP(

// # Comparators
// =============
#define CMP_SETUP(cmp_token, expr_a, expr_b) (SCOPED_ONLY_CODEGEN(COMPARATOR, { \
    Value _value_a = {0}; \
    SEEKABLE_SCOPE( \
        SEEK_INDEX(1) SCOPED_EXPR_CODEGEN(expr_a) \
        _value_a = _expr_result; \
        SEEK_INDEX(2) SCOPED_EXPR_CODEGEN(expr_b) \
        _expr_result = value_make_number(value_get_double(_value_a) cmp_token value_get_double(_expr_result)); \
    ) \
})) ) // unbalanced

#define cmp CMP_SETUP(

// # Not
// =====
#define NOT_SETUP(empty, expr) (SCOPED_ONLY_CODEGEN(NOT, \
    SCOPED_EXPR_CODEGEN(expr); \
    _expr_result = value_make_bool(!value_get_bool(_expr_result)); \
)) ) // unbalanced

#define not NOT_SETUP(

// # Construct pair
// ================
#define CONS_SETUP(empty, expr_a, expr_b) (SCOPED_ONLY_CODEGEN(CONS, { \
    Value _cons_a = {0}; \
    SEEKABLE_SCOPE( \
        SCOPED_EXPR_CODEGEN(expr_a) \
        _cons_a = _expr_result; \
        SCOPED_EXPR_CODEGEN(expr_b) \
        _expr_result = value_make_pair(MAKE((struct Pair) {.x = _cons_a, .y = _expr_result})); \
    ) \
})) ) // unbalanced

#define cons CONS_SETUP(

// # car (access first)
// ====================
#define CAR_SETUP(empty, expr) (SCOPED_ONLY_CODEGEN(CAR, \
    SCOPED_EXPR_CODEGEN(expr); \
    if (_expr_result.tag != VALUE_PAIR) panic("value not a pair"); \
    _expr_result = _expr_result.data.pair->x; \
)) ) // unbalanced

#define car CAR_SETUP(

// # cdr (access second)
// =====================
#define CDR_SETUP(empty, expr) (SCOPED_ONLY_CODEGEN(CDR, \
    SCOPED_EXPR_CODEGEN(expr) \
    if (_expr_result.tag != VALUE_PAIR) panic("value not a pair"); \
    _expr_result = _expr_result.data.pair->y; \
)) ) // unbalanced

#define cdr CDR_SETUP(

// # Format string
// ===============
// Some of these macros are also used by print and println
#define FMT_SETUP_ARGUMENT(expr, index) \
    SEEK_INDEX(EVIL_INC_##index) \
        SCOPED_EXPR_CODEGEN(expr) \
        _fmt_arguments[index] = _expr_result;

#define FMT_VALIDATE_ARGUMENT(expr, index) , ((const char*)0)

#define FMT_SETUP_INTERNAL(fmt_str, code, ...) \
    SEEKABLE_SCOPE( \
        Value _fmt_arguments[EVIL_COUNT(__VA_ARGS__)]; \
        THII_MAP_ARGUMENTS((FMT_SETUP_ARGUMENT), __VA_ARGS__) \
        EVIL_EXPAND code \
        if (0) { \
            /* Compile-time validation is done with a printf that is never called */ \
            printf(fmt_str \
                THII_MAP_ARGUMENTS((FMT_VALIDATE_ARGUMENT), __VA_ARGS__) \
            ); \
        } \
    ) \

#define FMT_SETUP(empty, fmt_str, ...) (SCOPED_ONLY_CODEGEN(FMT, \
    FMT_SETUP_INTERNAL(fmt_str, ( \
        _expr_result = value_make_str(fmt_new_string(fmt_str "", _fmt_arguments, EVIL_COUNT(__VA_ARGS__))); \
    ), __VA_ARGS__) \
)) ) // unbalanced

#define fmt FMT_SETUP(

// # Print
// =======
#define PRINT_SETUP(empty, fmt_str, ...) (SCOPED_ONLY_CODEGEN(PRINT, \
    FMT_SETUP_INTERNAL(fmt_str, ( \
        print_fmt_values(fmt_str "", _fmt_arguments, EVIL_COUNT(__VA_ARGS__)); \
        _expr_result = nil; \
    ), __VA_ARGS__) \
)) ) // unbalanced

#define print PRINT_SETUP(

// # Print line
// ============
#define PRINTLN_SETUP(empty, fmt_str, ...) (SCOPED_ONLY_CODEGEN(PRINTLN, \
    FMT_SETUP_INTERNAL(fmt_str, ( \
        print_fmt_values(fmt_str "\n", _fmt_arguments, EVIL_COUNT(__VA_ARGS__)); \
        _expr_result = nil; \
    ), __VA_ARGS__) \
)) ) // unbalanced

#define println PRINTLN_SETUP(

// # Toplevel THII() macro
// =======================
#define THII(...) \
    TOPLEVEL_EXPR_CODEGEN((__VA_ARGS__)) \
    int main() { \
        ScopeInfo* _seek_scope = nullptr; \
        TOPLEVEL_DEFUN_CODEGEN_BODY(( \
            SCOPED_EXPR_CODEGEN((call, start)) \
            return (int)value_get_double(_expr_result); \
        )) \
    };

#endif // THII_H
