#include "thii.h"
THII(

(defun, myfunc1, (), (
    (defun, closure1, (), 0),
    closure1
)),

(defun, myfunc2, (), (0)),

(defun, start, (), (
    // stdout: nil: nil
    (println, "nil: %s", nil),
    // stdout: bools: true false
    (println, "bools: %s %s", true, false),
    // stdout: ints: 1 1e+10 -27
    (println, "ints: %s %s %s", 1, (op*, 100000, 100000), -27),
    // stdout: floats: 2.5 -1e-26
    (println, "floats: %s %s", 2.5, -10e-27),
    // stdout: strings: foo bar baz, newlines:
    // stdout:
    // stdout:
    // stdout:
    (println, "strings: %s, %s", "foo bar baz", "newlines:\n\n\n"),
    // stdout: strings with %p: "\"foo\"\n"
    (println, "strings with %%p: %p", "\"foo\"\n"),
    // stdout: pairs: (1, 2) ("foo", (("bar", "newlines: \n\n\n"), nil))
    (println, "pairs: %s %s", (cons, 1, 2.0), (cons, "foo", (cons, (cons, "bar", "newlines: \n\n\n"), nil))),
    // stdout: func: myfunc2()
    (println, "func: %s", (ifel, false, myfunc1, myfunc2)),
    // stdout: closure: myfunc1:closure1()
    (println, "closure: %s", (call, myfunc1)),
    // stdout: multiple prints on the same line
    (print, "multiple "),
    (print, "prints "),
    (print, "on the same line\n"),
    // stdout: format to string: 1 + 1 = 2
    (println, "format to string: %s", (fmt, "%s + %s = %s", 1, 1, (op+, 1, 1))),
    // stdout: can concat strings
    (println, "%s", (op+, "can concat ", "strings")),
    // stdout: can concat strings with numbers 7
    (println, "%s", (op+, "can concat strings with numbers ", 7)),
    0
)))
