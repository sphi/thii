#include <thii.h>
THII(
(defun, not_used, (a, b), (op+, a, b)),
(defun, square, (num), (call, mul, num, num)),
(defun, mul, (a, b), (op*, a, b)),
(defun, add, (a, b), (op+, a, b)),
(defun, start, (), (
    // stdout: 16
    (println, "%s", (call, square, 4)),
    // stdout: 32-postfix
    (println, "%s", (call, add, (call, mul, (call, square, 4), 2), "-postfix")),
    0
)))
