#include <thii.h>
THII(
(defun, start, (), (
    (let, l, (cons, false, (cons, "nested", 8.2))),
    // stdout: false
    (println, "%s", (car, l)),
    // stdout: nested
    (println, "%s", (car, (cdr, l))),
    // stdout: 8.2
    (println, "%s", (ifel, (car, l), (car, (cdr, l)), (cdr, (cdr, l)))),
    // stdout: nested
    (println, "%s", (ifel, (not, (car, l)), (car, (cdr, l)), (cdr, (cdr, l)))),
    0
)))
