#include <thii.h>
THII(
(defun, start, (), (
    (let, foo, nil),
#ifdef COMPILE_ERROR_1
    (println, "Hello!", foo),
#endif
#ifdef COMPILE_ERROR_2
    (println, "Hello! %s"),
#endif
#ifdef COMPILE_ERROR_3
    (println, "Hello! %p"),
#endif
#ifdef COMPILE_ERROR_4
    (println, "Hello! %s %s", foo),
#endif
#ifdef COMPILE_ERROR_5
    (println, "Hello! %p %p", foo, foo, foo),
#endif
#ifdef COMPILE_ERROR_6
    (println, "Hello! %d", foo),
#endif
#ifdef COMPILE_ERROR_7
    (println, "Hello! %ld", foo),
#endif
#ifdef COMPILE_ERROR_8
    (println, "Hello! %lld", foo),
#endif
#ifdef COMPILE_ERROR_9
    (println, "Hello! %zd", foo),
#endif
#ifdef COMPILE_ERROR_10
    (println, "Hello! %c", foo),
#endif
#ifdef COMPILE_ERROR_11
    (println, "Hello! %d %s", foo),
#endif
#ifdef COMPILE_ERROR_12
    (print, "Hello!\n", foo),
#endif
#ifdef COMPILE_ERROR_13
    (fmt, "Hello!\n %s %s", foo),
#endif
    (fmt, "Hello! %p", foo),
    // stdout: nil
    (println, "Hello! %s", foo),
    // stdout: nil
    (println, "Hello! %p", foo),
    // stdout: nil
    (print, "Hello! %p\n", foo),
    0
)))
