#include <thii.h>
THII(
(defun, real_fn, (), nil),
(defun, start, (), (

    (let, a,
#ifdef COMPILE_ERROR_let
     b
#else
     7
#endif
    ),

    (call,
#ifdef COMPILE_ERROR_call
     unkown_fn
#else
     real_fn
#endif
    ),

    0
)))
