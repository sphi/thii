#include <thii.h>
THII(
(defun, start, (), (
    // stdout: nil
    (println, "%s", first),
    (let, second, (op+, first, "...")),
    (let, first, "hello"),
    // stdout: hello
    (println, "%s", first),
    // stdout: nil...
    (println, "%s", second),
    0
)))
