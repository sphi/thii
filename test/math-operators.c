#include <thii.h>
THII(
(defun, start, (), (
    // stdout: 15
    (println, "%s", (op+, 3, 12)),
    // stdout: 15.5
    (println, "%s", (op+, 3, 12.5)),
    // stdout: -9
    (println, "%s", (op-, 3, 12)),
    // stdout: 36
    (println, "%s", (op*, 3, 12)),
    // stdout: 0.25
    (println, "%s", (op/, 3, 12)),
    // stdout: 2
    (println, "%s", (op%, 14, 3)),

    // stdout: 7
    (println, "%s", (op+, 3, (op*, 2, 2))),
    // stdout: 7
    (println, "%s", (op+, (op-, 4, 1), 4)),
    // stdout: 7
    (println, "%s", (op+, (op-, 4, 1), (op*, 2, 2))),

    // stdout: 1
    (println, "%s", (cmp==, 4, 4)),
    // stdout: 1
    (println, "%s", (cmp==, 4.5, 4.5)),
    // stdout: 0
    (println, "%s", (cmp==, 4, 4.5)),

    // stdout: 1
    (println, "%s", (cmp>, 5, 4.5)),
    // stdout: 0
    (println, "%s", (cmp>, 4.5, 4.5)),
    // stdout: 0
    (println, "%s", (cmp>, 4, 4.5)),

    // stdout: 0
    (println, "%s", (cmp<, -2, -3)),
    // stdout: 0
    (println, "%s", (cmp<, -3, -3)),
    // stdout: 1
    (println, "%s", (cmp<, -4, -3)),

    // stdout: 1
    (println, "%s", (cmp>=, 5, 4)),
    // stdout: 1
    (println, "%s", (cmp>=, 4, 4)),
    // stdout: 0
    (println, "%s", (cmp>=, 3, 4)),

    // stdout: 0
    (println, "%s", (cmp<=, 5, 4)),
    // stdout: 1
    (println, "%s", (cmp<=, 4, 4)),
    // stdout: 1
    (println, "%s", (cmp<=, 3, 4)),
    0
)))
