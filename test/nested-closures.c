#include "thii.h"
THII(

(defun, get_closure, (outer), (
    (println, "get_closure called: %s", outer),
    //(defun, middle2, (), 0),
    (let, foo, (op+, outer, (
        (defun, get_foo_outer, (), (
            (defun, get_foo_inner, (), (
                (println, "get_foo_inner() called"),
                (let, foo, "-foo"),
                foo
            )),
            (call, get_foo_inner)
        )),
        (call, get_foo_outer)
    ))),
    (defun, middle, (middle_arg), (
        (let, bar, (op+, foo, "-bar")),
        (println, "middle closure called: %s", middle_arg),
        (defun, inner1, (a, b), (
            (println, "inner closure 1 called: %s, %s", a, b),
            (op+, bar, a)
        )),
        (ifel, middle_arg, inner1, (
            (defun, inner2, (a, b), (
                (println, "inner closure 2 called: %s, %s", a, b),
                (op+, bar, b)
            )),
            inner2
        ))
    )),
    middle
)),

(defun, start, (), (
    // stdout: get_closure called: outer
    // stdout: get_foo_inner() called
    (let, a, (call, get_closure, "outer")),
    // stdout: middle closure called: true
    (let, b, (call, a, true)),
    // stdout: middle closure called: false
    (let, c, (call, a, false)),
    // stdout: get_closure called: outer2
    // stdout: get_foo_inner() called
    (let, d, (call, get_closure, "outer2")),
    // stdout: inner closure 1 called: -a, -b
    // stdout: outer-foo-bar-a
    (println, "%s", (call, b, "-a", "-b")),
    // stdout: inner closure 2 called: -c, -d
    // stdout: outer-foo-bar-d
    (println, "%s", (call, c, "-c", "-d")),
    0
)))
